#!/bin/sh
#  SPDX-License-Identifier: MIT
#
#  EmulationStation Desktop Edition
#  macOS_change_dylib_rpaths.sh
#
#  Updates the dylib paths to use rpaths instead of absolute paths.
#  This script does not do much error checking so the results should be verified manually
#  afterwards using the "otool -L" command.
#  Changes are needed to this script when moving to a new version for any of the libraries.
#

export FREETYPE_FILENAME=libfreetype.6.dylib
export AVCODEC_FILENAME=libavcodec.58.dylib
export AVFILTER_FILENAME=libavfilter.7.dylib
export AVFORMAT_FILENAME=libavformat.58.dylib
export POSTPROC_FILENAME=libpostproc.55.dylib
export SWRESAMPLE_FILENAME=libswresample.3.dylib
export SWSCALE_FILENAME=libswscale.5.dylib

if [ -f $FREETYPE_FILENAME ]; then
  echo Found file $FREETYPE_FILENAME - changing to rpaths
  chmod 755 $FREETYPE_FILENAME
  install_name_tool -change /usr/local/opt/libpng/lib/libpng16.16.dylib @rpath/libpng16.16.dylib $FREETYPE_FILENAME
  install_name_tool -add_rpath @executable_path $FREETYPE_FILENAME
fi

if [ -f $AVCODEC_FILENAME ]; then
  echo Found file $AVCODEC_FILENAME - changing to rpaths
  chmod 755 $AVCODEC_FILENAME
  install_name_tool -change /usr/local/lib/libswresample.3.dylib @rpath/libswresample.3.dylib $AVCODEC_FILENAME
  install_name_tool -change /usr/local/lib/libavutil.56.dylib @rpath/libavutil.56.dylib $AVCODEC_FILENAME
  install_name_tool -change /usr/local/opt/fdk-aac/lib/libfdk-aac.2.dylib @rpath/libfdk-aac.2.dylib $AVCODEC_FILENAME
  install_name_tool -add_rpath @executable_path $AVCODEC_FILENAME
fi

if [ -f $AVFILTER_FILENAME ]; then
  echo Found file $AVFILTER_FILENAME - changing to rpaths
  chmod 755 $AVFILTER_FILENAME
  install_name_tool -change /usr/local/lib/libavformat.58.dylib @rpath/libavformat.58.dylib $AVFILTER_FILENAME
  install_name_tool -change /usr/local/lib/libavcodec.58.dylib @rpath/libavcodec.58.dylib $AVFILTER_FILENAME
  install_name_tool -change /usr/local/lib/libpostproc.55.dylib @rpath/libpostproc.55.dylib $AVFILTER_FILENAME
  install_name_tool -change /usr/local/lib/libswscale.5.dylib @rpath/libswscale.5.dylib $AVFILTER_FILENAME
  install_name_tool -change /usr/local/lib/libswresample.3.dylib @rpath/libswresample.3.dylib $AVFILTER_FILENAME
  install_name_tool -change /usr/local/lib/libavutil.56.dylib @rpath/libavutil.56.dylib $AVFILTER_FILENAME
  install_name_tool -change /usr/local/opt/fdk-aac/lib/libfdk-aac.2.dylib @rpath/libfdk-aac.2.dylib $AVFILTER_FILENAME
  install_name_tool -add_rpath @executable_path $AVFILTER_FILENAME
fi

if [ -f $AVFORMAT_FILENAME ]; then
  echo Found file $AVFORMAT_FILENAME - changing to rpaths
  chmod 755 $AVFORMAT_FILENAME
  install_name_tool -change /usr/local/lib/libavcodec.58.dylib @rpath/libavcodec.58.dylib $AVFORMAT_FILENAME
  install_name_tool -change /usr/local/lib/libswresample.3.dylib @rpath/libswresample.3.dylib $AVFORMAT_FILENAME
  install_name_tool -change /usr/local/lib/libavutil.56.dylib @rpath/libavutil.56.dylib $AVFORMAT_FILENAME
  install_name_tool -change /usr/local/opt/fdk-aac/lib/libfdk-aac.2.dylib @rpath/libfdk-aac.2.dylib $AVFORMAT_FILENAME
  install_name_tool -add_rpath @executable_path $AVFORMAT_FILENAME
fi

if [ -f $POSTPROC_FILENAME ]; then
  echo Found file $POSTPROC_FILENAME - changing to rpaths
  chmod 755 $POSTPROC_FILENAME
  install_name_tool -change /usr/local/lib/libavutil.56.dylib @rpath/libavutil.56.dylib $POSTPROC_FILENAME
  install_name_tool -add_rpath @executable_path $POSTPROC_FILENAME
fi

if [ -f $SWRESAMPLE_FILENAME ]; then
  echo Found file $SWRESAMPLE_FILENAME - changing to rpaths
  chmod 755 $SWRESAMPLE_FILENAME
  install_name_tool -change /usr/local/lib/libavutil.56.dylib @rpath/libavutil.56.dylib $SWRESAMPLE_FILENAME
  install_name_tool -add_rpath @executable_path $SWRESAMPLE_FILENAME
fi

if [ -f $SWSCALE_FILENAME ]; then
  echo Found file $SWSCALE_FILENAME - changing to rpaths
  chmod 755 $SWSCALE_FILENAME
  install_name_tool -change /usr/local/lib/libavutil.56.dylib @rpath/libavutil.56.dylib $SWSCALE_FILENAME
  install_name_tool -add_rpath @executable_path $SWSCALE_FILENAME
fi
