//  SPDX-License-Identifier: MIT
//
//  EmulationStation Desktop Edition
//  HelpPrompt.h
//
//  Definition of the pair used by help prompts to display an icon and its mapped function.
//

#ifndef ES_CORE_HELP_PROMPT_H
#define ES_CORE_HELP_PROMPT_H

#include <string>

typedef std::pair<std::string, std::string> HelpPrompt;

#endif // ES_CORE_HELP_PROMPT_H
