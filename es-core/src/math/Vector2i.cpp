//  SPDX-License-Identifier: MIT
//
//  EmulationStation Desktop Edition
//  Vector2i.cpp
//
//  2-dimensional integer vector functions.
//

#include "math/Vector2i.h"
