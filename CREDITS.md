# EmulationStation Desktop Edition (ES-DE) - Credits

# Programming

Alec Lofquist (original version) \
http://www.aloshi.com

RetroPie Community (RetroPie fork) \
https://retropie.org.uk

Leon Styhre (Desktop Edition fork, based on the RetroPie version) \
https://gitlab.com/leonstyhre/emulationstation-de

The shader code for blur_horizontal.glsl, blur_vertical_glsl and scanlines.glsl has been borrowed from the [RetroArch](https://www.retroarch.com) project.


# UI Art & Design

Nils Bonenberger


# Licenses

Please find the individual license files inside the `licenses` directory. There is also additional license information in the headers of most source files.


# Libraries

CImg \
https://www.cimg.eu

cURL \
https://curl.haxx.se

FFmpeg \
https://ffmpeg.org

FreeType \
https://www.freetype.org

FreeImage \
http://www.freeimage.sourceforge.net

GLEW \
http://glew.sourceforge.net

libVLC \
https://wiki.videolan.org/LibVLC

nanosvg \
https://github.com/memononen/nanosvg

pugixml \
https://pugixml.org

RapdidJSON \
https://rapidjson.org

SDL \
https://www.libsdl.org


# Resources

Akrobat font \
https://www.fontfabric.com/fonts/akrobat

DejaVu font \
https://dejavu-fonts.github.io

DroidSans font \
https://android.googlesource.com/platform/frameworks/base

Font Awesome \
https://fontawesome.com

GNU FreeFont (FreeMono) \
https://www.gnu.org/software/freefont

Nanum font \
https://hangeul.naver.com

Ubuntu font \
https://design.ubuntu.com/font

MAME ROM information \
https://www.mamedev.org

CA certificates (for TLS/SSL support on Windows) \
https://wiki.mozilla.org/CA


# rbsimple-DE theme

Recalbox Multi (rbsimple-DE is based on this theme, using assets from before their change to a more restrictive license in 2018) \
https://gitlab.com/recalbox/recalbox-themes

Carbon (some graphics taken from this theme) \
https://github.com/RetroPie/es-theme-carbon


# Sounds

Used by the default rbsimple-DE theme as well as for fallback sounds (for themes that lack navigation sounds).

https://freesound.org/people/adcbicycle/sounds/14066

https://freesound.org/people/farpro/sounds/264762

https://freesound.org/people/farpro/sounds/264763/

https://freesound.org/people/newlocknew/sounds/515827 \
(Sample cut slightly)

https://freesound.org/people/ertfelda/sounds/243701/
