The EmulationStation theme 'rbsimple-DE' is based on 'recalbox-multi' by the Recalbox community. \
Some graphics was also taken from 'carbon' by Rookervik.


Recalbox credits
================
- Supernature2k
- Paradadf

Original work from the Recalbox community:
- Zuco
- Sm3ck
- Th4Shin
- Shub
- Lester
- MarbleMad
- Odissine
- Zarroun
- Reivaax
- RockAddicted
- Bigboo3000
- Paradadf


Other credits
=============
Theme 'carbon' v2.4 - 2016-08-16 by Rookervik, \
based on simple(c) Nils Bonenberger - nilsbyte@nilsbyte.de - http://blog.nilsbyte.de/
